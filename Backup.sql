use master
go

-----=======CREAR UN DISPOSITIVO PARA LOGICO PARA USARLO COMO BACKUP=========------------
EXEC sp_addumpdevice 'disk', 
'AdventureWorks2019',
'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\AW2019.bak';
go
-----------==================================================================-------------

--======LISTAR LOS DISPOSITIVOS
--SELECT * FROM sys.backup_devices
--go
--================================

--======ELIMINAR UN DISPOSITIVO
--EXEC sp_dropdevice 'AdventureWorks2019', 'delfile' ;  
--GO 
--==============================

--======CREAR UN BACKUP FULL DENTRO DE EL DISPOSITIVO CREADO ANTERIORMENTE
BACKUP DATABASE AdventureWorks2019
TO AdventureWorks2019
WITH FORMAT, INIT, NAME = N'AW2019 - Full Backup';
GO
--=================================================

--==LISTAR DONDE SE ENCUENTRAN LOS ARCHIVOS .MDF Y .LDF CON EL NOMBRE DEL DISPOSITIVO
RESTORE FILELISTONLY FROM AdventureWorks2019
GO
--==========
=================================

--==LISTAR LOS BACKUP CREADOS, TANTO FULL COMO DIFERENCIALES CON EL NOMBRE DEL DISPOSITIVO
RESTORE HEADERONLY FROM AdventureWorks2019
GO
---======================================
----

--==GUARDAR MULTIPLES BACKUP DENTRO DE EL DISPOSITIVO
DECLARE @BackupName VARCHAR(100)
SET @BackupName = N'AW2019 - Full Backup ' + FORMAT(GETDATE(),'yyyyMMdd_hhmmss');

BACKUP DATABASE AdventureWorks2019
TO AdventureWorks2019
WITH NOFORMAT, NOINIT, NAME = @BackupName,
SKIP, NOREWIND, NOUNLOAD, STATS = 10
GO
--====================================================
